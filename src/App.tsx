import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

import SidebarApp from './layout_components/sidebar/SidebarApp';

import { Routes, Route } from "react-router-dom";
import {Accueil} from './pages/Accueil';
import {VaisseauxPage} from './pages/VaisseauxPage';
import {PlanetesPage} from './pages/PlanetesPage';
import { ExoBiologiePage } from './pages/ExoBiologiePage';





export default function App() {
  return (
    <div className="displayPage d-flex">
          <Routes>
            <Route path="" element={<SidebarApp />}>
            <Route index element={<Accueil />} />
            <Route path="/vaisseaux" element={<VaisseauxPage />} />            
            <Route path="/planetes" element={<PlanetesPage/>} />   
            <Route path="/exobiologie" element={<ExoBiologiePage/>} />
            </Route>
        </Routes>
      
      </div>
  )
}

