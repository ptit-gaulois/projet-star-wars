import { useState, useEffect } from 'react';
import {BASE_URL_PLANETES} from "../../src/environment/env";
import { Planete } from '../interfaces/Planete';
import "./PlanetesPage.css"
import { PlaneteDetails } from './PlaneteDetails';
import { SearchBar } from '../layout_components/searchBar/SearchBar'

export function PlanetesPage() {
    const [planeteList, setPlaneteList] = useState<Planete[]>([]);

    useEffect(() => {
        const fetchAllPlanetes = async () => {
            let url = BASE_URL_PLANETES;
            let results: Planete[] = [];
            while (url) {
                const response = await fetch(url);
                const data = await response.json();
                results = results.concat(data.results);
                url = data.next; 
            }
            results.sort((a, b) => a.name.localeCompare(b.name));
            setPlaneteList(results);
        };

        fetchAllPlanetes().catch(error => console.error("Erreur au chargement des Planètes", error));
    }, []);

    return (
        <>
        <SearchBar planeteListSearch={planeteList} />
        <div className="grid-container">
            {planeteList.map((planete) => (
                <div className="card" key={planete.name}>
                      <PlaneteDetails PlanetInfo={planete} />                   
                </div>
            ))}
        </div>
        </>
    );
}
