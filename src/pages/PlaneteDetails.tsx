import React, { useState, useEffect } from 'react';

import { Planete } from '../interfaces/Planete';
import { Link } from 'react-router-dom';

interface planeteDetailsProps {
    PlanetInfo: Planete;
}



export const PlaneteDetails: React.FC<planeteDetailsProps> = ({PlanetInfo}) => {
   

    console.log(PlanetInfo)

    return (
        <div style={{ color: 'rgb(246, 186, 6)', textDecoration: 'none' }}>
            <Link to={`${PlanetInfo.name}`}>
            <h1>{PlanetInfo.name}</h1>
            <p>Population : {PlanetInfo.population}</p>
            <p>Filmographie : {PlanetInfo.films.join(', ')}</p>
            </Link>
        </div>
    );
};


