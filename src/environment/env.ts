
// Exportation de l'URL de base pour les planètes de l'API SWAPI
export const BASE_URL_PLANETES: string = 'http://swapi.dev/api/planets';

// Exportation par défaut de l'objet contenant BASE_URL_PLANETES
export default { BASE_URL_PLANETES };
