
import { Link, Outlet } from 'react-router-dom'; // Importez le composant Link
import { YellowButton } from '../buttons/YellowButton';
import './SidebarApp.css'

const SidebarApp= () => {
    return (
        <>
        <div className="Sidebar ps-5 pe-5">
        <ul>
            <li className="sidebarButton">
                <Link to="/">
                    <YellowButton text="Accueil" />
                </Link>
            </li>
            <li>
                <Link to="/planetes">
                    <YellowButton text="Planètes" />
                </Link>
            </li>
            <li>
                <Link to="/vaisseaux">
                    <YellowButton text="Vaisseaux" />
                </Link>
            </li>
            <li>
                <Link to="/exobiologie">
                    <YellowButton text="Exo-Biologie" />
                </Link>
            </li>
        </ul>
        </div>
        <Outlet />
        </>
    );
}

export default SidebarApp
