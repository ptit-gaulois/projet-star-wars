import "./YellowButton.css"


interface YellowButtonProps {
    text: string;
}

export const YellowButton: React.FC<YellowButtonProps> = ({ text }) => {
    return (
        <button className="btn btn-warning yellowButton">
            {text}
        </button>
    );
}
