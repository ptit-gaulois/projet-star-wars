import { useState } from "react";
import { Planete } from "../../interfaces/Planete";
import "./Searchbar.css";

interface SearchProps {
    planeteListSearch: Planete[]; 
}

export const SearchBar: React.FC<SearchProps> = ({ planeteListSearch }) => {
    const [searchTerm, setSearchTerm] = useState(""); // Pour stocker le terme de recherche

    // Fonction pour filtrer la liste des planètes en fonction du terme de recherche
    const filterPlaneteList = (searchString: string) => {
        return planeteListSearch.filter(planete => 
            planete.name.toLowerCase().includes(searchString.toLowerCase())
        );
    };

    // Gestionnaire d'événements pour mettre à jour le terme de recherche et filtrer la liste
    const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const searchString = event.target.value;
        setSearchTerm(searchString); // Mettre à jour le terme de recherche
        const filteredList = filterPlaneteList(searchString); // Filtrer la liste des planètes
        // Vous pouvez maintenant utiliser `filteredList` pour afficher les résultats de la recherche
    };

    return (
        <input
            type="text"
            placeholder="votre recherche"
            value={searchTerm}
            onChange={handleSearchChange}
        />
    );
}
